import logging
import sys

# Configure stdout logger
stdout_log = logging.getLogger('stdout_logger')
stdout_log.setLevel(logging.DEBUG)
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.DEBUG)

stdout_formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s')
stdout_handler.setFormatter(stdout_formatter)
stdout_log.addHandler(stdout_handler)


# Configure the 'fw_classification' logger
fw_classification_logger = logging.getLogger('fw_classification')
fw_classification_logger.setLevel(logging.DEBUG)

# Add stdout_handler as a handler for the 'fw_classification' logger
fw_classification_logger.addHandler(stdout_handler)

# Configure stderr logger
stderr_log = logging.getLogger('stderr_logger')
stderr_log.setLevel(logging.DEBUG)
stderr_handler = logging.StreamHandler(sys.stderr)
stderr_handler.setLevel(logging.DEBUG)

stderr_formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s')
stderr_handler.setFormatter(stderr_formatter)
stderr_log.addHandler(stderr_handler)


