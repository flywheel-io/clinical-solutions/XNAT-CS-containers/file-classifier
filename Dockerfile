FROM alpine/git:2.40.1 as profiles

ENV PROFILE_VERSION=0.4.3

RUN git clone --depth 1 \
    --branch $PROFILE_VERSION \
    https://gitlab.com/flywheel-io/public/fw-classification/fw-classification-profiles.git \
    /root/profiles/


FROM python:3.11.6-slim-bullseye AS deps
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN apt-get update &&  \
    apt-get install --no-install-recommends -y git=1:2.30.2-1+deb11u2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/

# Copying profiles
COPY --from=profiles /root/profiles/classification_profiles ./fw_gear_file_classifier/classification_profiles

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]


LABEL org.nrg.commands="[{\"name\": \"file-classifier\", \"label\": \"file-classifier\", \"description\": \"Runs file-classifier v1.1\", \"version\": \"1.1\", \"schema-version\": \"1.0\", \"image\": \"registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/file-classifier:1.1\", \"type\": \"docker\", \"command-line\": \"python run.py #DEBUG# #PROFILE# #VALIDATE# #PROJECT#\", \"override-entrypoint\": true, \"mounts\": [{\"name\": \"in\", \"writable\": false, \"path\": \"/input\"}, {\"name\": \"out\", \"writable\": true, \"path\": \"/output\"}], \"environment-variables\": {}, \"ports\": {}, \"inputs\": [{\"name\": \"debug\", \"description\": \"Include debug statements in the output\", \"type\": \"boolean\", \"default-value\": \"false\", \"required\": false, \"replacement-key\": \"#DEBUG#\", \"true-value\": \"--debug\", \"false-value\": \"\"}, {\"name\": \"validate\", \"description\": \"Validate classification schema\", \"type\": \"boolean\", \"default-value\": \"false\", \"required\": false, \"replacement-key\": \"#VALIDATE#\", \"true-value\": \"--validate\", \"false-value\": \"\"}, {\"name\": \"profile\", \"description\": \"Classification profile\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROFILE#\", \"default-value\": \"main.yaml\", \"command-line-flag\": \"--profile\", \"command-line-separator\": \" \"}, {\"name\": \"project\", \"description\": \"project id\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROJECT#\", \"command-line-flag\": \"--project\", \"command-line-separator\": \" \"}], \"outputs\": [{\"name\": \"file-classifier\", \"description\": \"file-classifier\", \"required\": true, \"mount\": \"out\", \"glob\": \"*.json\"}], \"xnat\": [{\"name\": \"file-classifier\", \"label\": \"file-classifier-v1.1\", \"description\": \"Run file-classifier v1.1\", \"contexts\": [\"xnat:imageScanData\"], \"external-inputs\": [{\"name\": \"scan\", \"description\": \"Input scan\", \"type\": \"Scan\", \"required\": true, \"provides-files-for-command-mount\": \"in\", \"load-children\": false}], \"derived-inputs\": [{\"name\": \"session\", \"type\": \"Session\", \"required\": true, \"user-settable\": false, \"load-children\": true, \"derived-from-wrapper-input\": \"scan\", \"multiple\": false}, {\"name\": \"project\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"project\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"project-id\", \"multiple\": false}], \"output-handlers\": [{\"name\": \"file-classifier\", \"accepts-command-output\": \"file-classifier\", \"as-a-child-of\": \"scan\", \"type\": \"Resource\", \"label\": \"file-classifier\", \"tags\": []}]}], \"reserve-memory\": 500, \"container-labels\": {}, \"generic-resources\": {}, \"ulimits\": {}, \"secrets\": []}]"