import sys
from pathlib import Path
from typing import Any, Dict
import yaml
import xnat
import os

from .xnat_logging import stdout_log

work_dir="/flywheel/v0"
profile_path=os.path.join(work_dir,"fw_gear_file_classifier/classification_profiles")

def get_schema_definition(profile_path: Path) -> Dict:
    """Read yaml profile.

    Args:
        profile_path: path to yaml profile to read.

    Returns:
        dict:yaml profile converted to dictionary.
    """
    yaml_file = profile_path.as_posix()
    with open(yaml_file, "r", encoding="utf8") as file:
        yaml_dict = yaml.load(file, Loader=yaml.FullLoader)

    return yaml_dict


def validate_modality_schema(file_header: Dict[str, Any]) -> None:
    """Validate modality against predefined modality schema.

    Validate if the modality of the file input matches the predefined modality schema
    in the classification profile. Raises otherwise.

    Args:
        file_obj (Dict[str, Any]): file input object.
    """


    file_modality=file_header["file"].get("modality")


    # Get default schema
    default_schema_path = Path(profile_path) / "fw-modality-classification.yaml"
    classification_schema = get_schema_definition(default_schema_path)

    predefined_schema = classification_schema.get(file_modality)
    if not predefined_schema:  # in case we don't have a schema for this modality
        stdout_log.error(f"modality {file_modality} unknown. No schema defined.")
        sys.exit(1)
 
def get_project_info(project:str,profile:str)->None:
    """ Gets custom project classification & user defined profile
    project : str
        project id
    profile : str
        profile name to search for in project resources
    """

    host=os.environ["XNAT_HOST"]
    user=os.environ["XNAT_USER"]
    password=os.environ["XNAT_PASS"]

    session = xnat.connect(host, user=user, password=password)
    proj_resources=session.get_json(f"/data/projects/{project}/files")

    filenames = ['classification.json', profile]
   
    # look for the specified files in project resource files
    for result in proj_resources['ResultSet']['Result']:
        if result['Name'] in filenames:
            file_uri=result['URI']
            if file_uri:
                session.download(file_uri, f"/{profile_path}/{result['Name']}")

                if os.path.exists(f"/{profile_path}/{result['Name']}"):
                    stdout_log.info(f"Found and copied {result['Name']} to {profile_path}")
                else:
                    stdout_log.error("Unable to copy {result['Name']} to {profile_path}")
                    
    session.disconnect()