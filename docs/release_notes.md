# Release notes
**1.1**
- Corresponds to File Classifier Gear v0.6.5
- **Enhancements**:
  - Update classification profiles to v0.4.3
    - adds in CT classification profile
    - updates MR profile to be more robust by using manufacturer-specific and physics based parameters

**1.0**
- Initial release corresponding to File Classifier Gear v0.6.1