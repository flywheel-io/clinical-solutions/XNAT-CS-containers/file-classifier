import argparse
import logging
import os
import sys
import json

from typing import Tuple
from .utils import get_project_info

from fw_classification.classify import Profile
from fw_classification.classify.block import Block

from .xnat_logging import stdout_log,fw_classification_logger

work_dir="/flywheel/v0"
profile_path=os.path.join(work_dir,"fw_gear_file_classifier/classification_profiles")

parser = argparse.ArgumentParser(description='File-Classifier')
parser.add_argument('--debug',action='store_true',required=False)
parser.add_argument('--profile',required=False,default="main.yaml")
parser.add_argument('--validate',action='store_true',required=False)
parser.add_argument('--project',required=True)

args=parser.parse_args() 

def parse_config() -> Tuple[Profile, bool]:  # File input  # Profile to classify with
    """Parse options from gear config.

    Args:
        gear_context (GearToolkitContext): Gear toolkit context.

    Returns:
        tuple:
            - File input as a dictionary
            - Profile to use for classification, defaults to the
                classification-toolkits "main.yml"
    """
    
    # debug
    if not args.debug:
        stdout_log.info("Disabling debug level logs")
        stdout_log.setLevel(logging.INFO)
        fw_classification_logger.setLevel(logging.INFO)

    validate=args.validate

    get_project_info(args.project,args.profile)
    
    return validate

def build_profile()->Profile:
    """ Build the classification profile
    Returns:
        Profile
        - returns the classification profile. 
        - If there was a custom classification provided at the project level, that
        gets added as an additional Block
    """
    # classification profile 
    if args.profile in os.listdir(profile_path):
        profile=Profile(os.path.join(profile_path,args.profile))
        stdout_log.info(f"Profile being used: {args.profile}")
    else:
        stdout_log.error(f"{args.profile} is an invalid profile.")
        sys.exit(1)
    
    # add in custom project classification to the profile if it was found
    if os.path.exists(f"/{profile_path}/classification.json"):
        stdout_log.info(f"Found a project classification.json as a project resource in {args.project}:")

        with open(f"/{profile_path}/classification.json",'r') as fp:
            custom_classification = json.load(fp)


        # add in the custom classification to the profile as a block
        block = {"name": "custom", "rules": custom_classification.get("classifications")}
        custom_block,err=Block.from_dict(block)
        stdout_log.info(custom_block)

        profile.handle_block(custom_block,prof_name="custom",block_key="key")
    



    return profile
