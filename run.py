#!/usr/bin/env python
"""The run script."""
import traceback
import os
import json
import sys
from fw_gear_file_classifier.parser import parse_config,build_profile
from fw_gear_file_classifier.utils import validate_modality_schema
from fw_classification.classify import run_classification
from fw_gear_file_classifier.xnat_logging import stdout_log, stderr_log

def main() -> None:
    """Parse config and run."""
    # Parse config
    validate = parse_config()
   
    # get the header JSON from file-metadata-extractor
    if not os.path.exists("/input/file-metadata-extractor"):
        stdout_log.error("file-metadata-extractor not found under the scan resource. "
                   "Running file-metadata-extractor is a pre-requisite to this container")
        sys.exit(1)
    
    # only DICOM and NIFTI supported
    for file_type in os.listdir("/input"):
        # catch any errors and provide traceback
        try:
            if file_type == "DICOM":
                file_header_file=f"/input/file-metadata-extractor/{file_type}_header.json"
                
                if os.path.exists(file_header_file):
                    with open(file_header_file, "r") as json_file:
                        file_header = json.load(json_file)
                    
                    if validate:
                        validate_modality_schema(file_header)
                        
                    stdout_log.info("------------------------------------")
                    stdout_log.info("Classifying DICOM")
                    
                    profile=build_profile()

                    result, classification_dict = run_classification(profile, file_header)
                    
                    if "classification" in classification_dict["file"].keys():
                        classifcation=classification_dict["file"]["classification"]
                    
                        classification_savepath=f"/output/{file_type}_classification.json"
                        with open(classification_savepath, "w") as json_file:
                            json.dump({"classification":classifcation}, json_file)
                            
                        if os.path.exists(classification_savepath):
                            stdout_log.info(f"Saved {classification_savepath}")
                    else:
                        stdout_log.error(f"Unable to classify {file_type}")
                        sys.exit(1)
                    
            elif file_type == "NIFTI":
                # Get the JSON sidecar + format it for fw_classification
                for file in os.listdir("/input/NIFTI"):
                    if file.endswith(".json"):
                        side_car_file=f"/input/NIFTI/{file}"
                
                        with open(side_car_file, "r") as json_file:
                            side_car_dict = json.load(json_file)
                            
                            file_header = {"file": {"type":"nifti","modality":side_car_dict.get("Modality",""),"info": {"header": {"dicom": side_car_dict}}}} 
                            
                stdout_log.info("------------------------------------")
                stdout_log.info("Classifying NIFTI")
                profile=build_profile()

                if validate:
                    validate_modality_schema(file_header)
                    
                result, classification_dict = run_classification(profile, file_header)
      
                if "classification" in classification_dict["file"].keys():
                    classifcation=classification_dict["file"]["classification"]
                
                    classification_savepath=f"/output/{file_type}_classification.json"
                    with open(classification_savepath, "w") as json_file:
                        json.dump({"classification":classifcation}, json_file)   
                    
                    if os.path.exists(classification_savepath):
                        stdout_log.info(f"Saved {classification_savepath}")
                    
                else:
             
                   stdout_log.error(f"Unable to classify {file_type}")
                   sys.exit(1)

        except Exception as exp:
            traceback_info = traceback.format_exc()
            stdout_log.error(f"There was an issue with classifying {file_type} for this scan: %s Check stderr.log for more details. \n",exp)

            stderr_log.error(f"There was an issue with classifying {file_type} for this scan: %s %s \n",exp,traceback_info)
            sys.exit(1) # give a Failed status


if __name__ == "__main__":
    main()
